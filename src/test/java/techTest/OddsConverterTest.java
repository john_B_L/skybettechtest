package techTest;

import static org.junit.Assert.*;
import org.junit.Test;

public class OddsConverterTest {

	@Test
	public void testFractionalToDecimal41() {
		Odds o = new Odds();
		o.setNumerator(4);
		o.setDenominator(1);
		assertEquals(o.toDecimal(), 5, 0.001);
	}

	@Test
	public void testDecimalToFractional41() {
		Odds o = Odds.fromDecimal(5);
		assertEquals(o.getNumerator(), 4);
		assertEquals(o.getDenominator(), 1);
	}
	
	@Test
	public void testFractionalToDecimal14() {
		Odds o = new Odds();
		o.setNumerator(1);
		o.setDenominator(4);
		assertEquals(o.toDecimal(), 1.25, 0.001);
	}

	@Test
	public void testDecimalToFractional14() {
		Odds o = Odds.fromDecimal(1.25);
		assertEquals(o.getNumerator(), 1);
		assertEquals(o.getDenominator(), 4);
	}
}
