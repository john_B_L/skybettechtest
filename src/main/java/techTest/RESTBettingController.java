package techTest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
//import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
public class RESTBettingController {

    private static final String url = "http://skybettechtestapi.herokuapp.com/";

    @RequestMapping(value = "/available", method = RequestMethod.GET)
    public @ResponseBody DecimalOdds[] available() {
        // get the data
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<FractionalOdds[]> responseEntity = restTemplate.getForEntity((url+"available"), FractionalOdds[].class);
        FractionalOdds[] objects = responseEntity.getBody();

        // loop through FractionalOdds, convert to DecimalOdds
        DecimalOdds[] res = new DecimalOdds[objects.length];

        for (int i = 0; i < res.length; i++) {
            res[i] = new DecimalOdds(objects[i].getBet_id(), objects[i].getEvent(), objects[i].getName(), objects[i].getOdds().toDecimal());
        }

        return res;
    }

    // curl -i -H "Content-Type: application/json" -X POST -d "{\"bet_id\":1, \"odds\":{\"numerator\":10, \"denominator\":1}, \"stake\":10}" http://skybettechtestapi.herokuapp.com/bets
    // curl -i -H "Content-Type: application/json" -X POST -d "{\"bet_id\":4, \"odds\":2.75, \"stake\":10}" http://localhost:8080/bets
    @RequestMapping(value = "bets", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> bets(@RequestBody DecimalBet bet) {
        // prepare the data request
        Bet to_post = new Bet(bet.getBet_id(), Odds.fromDecimal(bet.getOdds()), bet.getStake());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Bet> request = new HttpEntity<>(to_post, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response;

        ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
            // send the data
            response = restTemplate.exchange(url+"bets", HttpMethod.POST, request, String.class);

            if (!response.getStatusCode().is2xxSuccessful()) {
                ObjectNode errorNode = mapper.createObjectNode();
                errorNode.put("error", response.getBody());
                json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(errorNode);
                return new ResponseEntity(json, response.getStatusCode());
            }

            // convert to JSON string with decima odds
            ReceiptFractional init = mapper.readValue(response.getBody(), ReceiptFractional.class);
            ObjectNode res = mapper.createObjectNode();
            res.put("bet_id", init.getBet_id());
            res.put("event", init.getEvent());
            res.put("name", init.getName());
            res.put("odds", init.getOdds().toDecimal());
            res.put("stake", init.getStake());
            res.put("transaction_id", init.getTransaction_id());

            json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(res);
            return new ResponseEntity(json, response.getStatusCode());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
