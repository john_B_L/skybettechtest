package techTest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DecimalBet {

	private long bet_id;
	private double odds;
    private double stake;

	public void setBet_id(long bet_id) {
		this.bet_id = bet_id;
	}

	public void setStake(double stake) {
		this.stake = stake;
	}

	public void setOdds(double odds) {
		this.odds = odds;
	}

	public long getBet_id() {
		return bet_id;
	}

	public double getStake() {
		return stake;
	}

	public double getOdds() {
		return odds;
	}

}
