package techTest;

public class DecimalOdds {

	private long bet_id;
	private String event;
	private String name;
	private double odds;

	public DecimalOdds(long bet_id, String event, String name, double odds) {
		this.bet_id = bet_id;
		this.event = event;
		this.name = name;
		this.odds = odds;
	}

	public long getBet_id() {
		return bet_id;
	}

	public String getEvent() {
		return event;
	}

	public String getName() {
		return name;
	}

	public double getOdds() {
		return odds;
	}

}
