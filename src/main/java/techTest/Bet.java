package techTest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Bet {

	private long bet_id;
	private Odds odds;
    private double stake;

	public Bet(long bet_id, Odds odds, double stake) {
		this.bet_id = bet_id;
		this.stake = stake;
		this.odds = odds;
	}

	public void setBet_id(long bet_id) {
		this.bet_id = bet_id;
	}

	public void setStake(double stake) {
		this.stake = stake;
	}

	public void setOdds(Odds odds) {
		this.odds = odds;
	}

	public long getBet_id() {
		return bet_id;
	}

	public double getStake() {
		return stake;
	}

	public Odds getOdds() {
		return odds;
	}

}
