package techTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechTestConfiguration {

    public static void main(String[] args) {
        SpringApplication.run(TechTestConfiguration.class, args);
    }
}