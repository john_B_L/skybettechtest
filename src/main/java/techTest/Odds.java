package techTest;

// used for GCD
import java.math.BigInteger;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Odds {
	private int numerator;
	private int denominator;

    public int getNumerator() {
        return this.numerator;
    }

    public int getDenominator() {
        return this.denominator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public double toDecimal() {
    	return 1 + ((double) numerator / denominator);
    }

    public static Odds fromDecimal(double odds) {
    	Odds o = new Odds();

    	double reduced = odds - 1;

    	String[] parts = String.valueOf(reduced).split("\\.");
    	long denom = (long) Math.pow(10, parts[1].length());
    	long numer = Long.valueOf(parts[0]) * denom + Long.valueOf(parts[1]);

    	// get the GCD
    	BigInteger gcd = BigInteger.valueOf(denom).gcd(BigInteger.valueOf(numer));

    	int i_gcd = gcd.intValue();
    	o.setNumerator((int) (numer / i_gcd));
    	o.setDenominator((int) (denom / i_gcd));

    	return o;
    }
}
