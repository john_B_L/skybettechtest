package techTest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FractionalOdds {

	private long bet_id;
	private String event;
	private String name;
	private Odds odds;

	public void setBet_id(long bet_id) {
		this.bet_id = bet_id;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOdds(Odds odds) {
		this.odds = odds;
	}

	public long getBet_id() {
		return bet_id;
	}

	public String getEvent() {
		return event;
	}

	public String getName() {
		return name;
	}

	public Odds getOdds() {
		return odds;
	}

}
