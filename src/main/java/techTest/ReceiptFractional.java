package techTest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceiptFractional {
    private long bet_id;
    private String event;
    private String name;
    private Odds odds;
    private double stake;
    private long transaction_id;

    public void setBet_id(long bet_id) {
        this.bet_id = bet_id;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOdds(Odds odds) {
        this.odds = odds;
    }

    public void setStake(double stake) {
        this.stake = stake;
    }

    public void setTransaction_id(long transaction_id) {
        this.transaction_id = transaction_id;
    }

    public long getBet_id() {
        return this.bet_id;
    }

    public String getEvent() {
        return this.event;
    }

    public String getName() {
        return this.name;
    }

    public Odds getOdds() {
        return this.odds;
    }

    public double getStake() {
        return this.stake;
    }

    public long getTransaction_id() {
        return this.transaction_id;
    }
}
