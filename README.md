####Sky Bet Tech Test, REST API
######John Lilleker

This project has been built using Spring and Gradle.

To build it, run `./gradlew build` on \*nix, or `gradlew clean build` on Windows
To run the server, `java -jar build/libs/techTest-0.1.0.jar`

To test the server GET request, run `curl -i localhost:8080/available`

To test the server POST request on terminal/cmd, run `curl -i -H "Content-Type: application/json" -X POST -d "{\"bet_id\":ID, \"odds\":ODDS, \"stake\":STAKE}" http://localhost:8080/bets` NOTE: the JSON format is different on \*nix systems, it would be `'{"bet_id":ID, "odds":ODDS, "stake":STAKE}'`
